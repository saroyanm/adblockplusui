/title Updates page for %MILESTONE

## Background

Ticket to update the [update page](/eyeo/specs/spec/spec/abp/updates.md) for the %MILESTONE release.

## List of Improvements

(Add relevant changes to this list)

- #TBA

## List of Fixes

(Add relevant fixes to this list)

- #TBA

## What to change

- **Hero image in header:** (Add link to the image)
- **List of Improvements:** (List improvements based on [List of Improvements](#list-of-improvements) above. List Title, Description, Link (optionally) and Image (optionailly))
  - TBA
- **List of Fixes:** (List fixes based on [List of Fixes](#list-of-fixes) above. List Title, Description, Link (optionally) and Image (optionally))
  - TBA

/milestone %MILESTONE
